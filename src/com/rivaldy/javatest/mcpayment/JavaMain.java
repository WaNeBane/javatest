package com.rivaldy.javatest.mcpayment;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class JavaMain {
	
	public int[] subtractedInt(int[] nums) {
		if(nums==null)
			return null;
		
		Set<Integer> loop = new LinkedHashSet<>();
		for (int i = 0; i < nums.length; i++) {
			for (int j = 0; j < nums.length; j++) {
				if(j==i)
					continue;
				if(nums[i]-nums[j]<0) {
					loop.add(i);
				}
			}
		}
		
		int[] result = new int[nums.length - loop.size()];
		int a = 0;
		for (int i = 0; i < nums.length; i++) {
			if(!loop.contains(i)) {
				result[a] = nums[i];
				a++;
			}
		}
		
		return result;
	}
	
	public int[] dividedInt(int[] nums, int x) {
		if(nums==null || x <= 0)
			return null;
		
		Set<Integer> loop = new LinkedHashSet<>();
		for (int i = 0; i < nums.length; i++) {
			for (int j = 0; j < nums.length; j++) {
				if(j==i)
					continue;
				if(nums[i]/nums[j]==x) {
					loop.add(i);
				}
			}
		}
		
		int[] result = new int[nums.length - loop.size()];
		int a = 0;
		for (int i = 0; i < nums.length; i++) {
			if(!loop.contains(i)) {
				result[a] = nums[i];
				a++;
			}
		}
		
		return result;
	}
	
	public String[] containStr(String word, int x) {
		if(word==null || x <= 0)
			return null;
		
		Set<Integer> loop = new LinkedHashSet<>();
		String[] words = word.split(" ");
		for (int i = 0; i < words.length; i++) {
			if(words[i].length()==x)
				loop.add(i);
		}
		
		String[] result = new String[loop.size()];
		int a = 0;
		for (int i = 0; i < words.length; i++) {
			if(loop.contains(i)) {
				result[a] = words[i];
				a++;
			}
		}
		return result;
	}
}
