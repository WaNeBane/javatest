package com.rivaldy.javatest.mcpayment.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.rivaldy.javatest.mcpayment.JavaMain;

class JavaTest {
	
	JavaMain main = new JavaMain();

	@Test
	public void testSubtractedInt() {
		int[] input = {3,1,4,2};
		int[] expected = {4};
		assertEquals(Arrays.toString(expected), Arrays.toString(main.subtractedInt(input)));
	}
	
	@Test
	public void testDividedInt() {
		int[] input = {1,2,3,4};
		int x = 4;
		int[] expected = {1,2,3};
		assertEquals(Arrays.toString(expected), Arrays.toString(main.dividedInt(input, x)));
	}
	
	@Test
	public void testcontainStr() {
		String word = "souvenir loud four other lost";
		int x = 4;
		String[] expected = {"loud", "four", "lost"};
		assertEquals(Arrays.toString(expected), Arrays.toString(main.containStr(word, x)));
	}

}
